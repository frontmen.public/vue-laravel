FROM composer:1.9
RUN docker-php-ext-install mysqli pdo pdo_mysql

ENTRYPOINT ["composer", "install",  "--no-dev"]
