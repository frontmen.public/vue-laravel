<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserProfile extends Model
{
    
    protected $table = 'user_profile';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    
    protected $fillable = [
        'phone',
        'second_name',
        'middle_name',
        'gender',
        'sity',
        'street',
        'building',
        'flat',
        'birth_date',
        'accept'
    ];
    
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
