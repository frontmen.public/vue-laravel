<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Feedback extends Model
{
    protected $table = 'feedback';

    
    protected $fillable = [
        'user_id',
        'topic_id',  
        'text',
        'name',
        'email',
        'phone',
        'status',
    ];

    public function topic() {
        return $this->hasOne(Topic::class);
    }
}
