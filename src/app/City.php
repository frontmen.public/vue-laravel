<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    protected $table = 'city';

    protected $fillable = [
        'uuid',
        'city_id',
        'region_id',
        'region_uuid',
        'name_ua',
        'name_en',
        'name_ru',
    ];

    public function region()
    {
        return $this->belongsTo(Region::class, 'region_uuid', 'uuid');
    }

    public function departments()
    {
        return $this->hasMany(Department::class, 'city_uuid', 'uuid');
    }
}
