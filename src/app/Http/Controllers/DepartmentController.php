<?php

namespace App\Http\Controllers;

use App\Status;
use Illuminate\Http\Request;
use App\Department;

class DepartmentController extends Controller
{
    public function index()
    {
        return Department::orderBy('department_no', 'ASC')
            ->with('city')
            ->with('region')
            ->get();
    }
}
