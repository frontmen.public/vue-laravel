<?php

namespace App\Http\Controllers;

use App\Order;
use App\Status;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

/**
 * Class OrderController
 * @package App\Http\Controllers
 */
class OrderController extends Controller
{
    /**
     * @param Request $request
     *
     * @return array
     */
    public function calculate(Request $request)
    {
        try{
            $response = (new FJn)->calc_ew_price($request->all());
            if( $response->status ){
                return response()->json($response);
            }
        } catch (\Exception $e){}

        return response()->json('Не вдалося знайти тариф для зазначених параметрів', 400);
//        return response()->json('Не вдалось розрахувати вартiсть', 400);
    }

    /**
     *
     * @param Request $request
     *
     * @return array
     */
    public function create(Request $request)
    {
        try {
            $order = Order::create([
                'body' => json_encode($request->all())
            ]);
            $response = (new FJn)->create_ew_с2с($request->all(), $order->id);
            if ($response->status) {
                $order->update(['number' => $response->result->data->ew_number]);
                return [
                    'ttn' => $order->number,
                    'number' => $order->number
                ];
            }
        } catch (\Exception $e) {}

        $msg = $response->msg->ua ?? 'Не вдалося створити посилку';

        return response()->json($msg, 400);
    }

    /**
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function list(Request $request)
    {
        $filters = $request->get('filters', []);
        $sorting = $request->get('sorting', []);
        $page = $request->get('page', 1);

        $phone = preg_replace('/[^\d+]/', '', session('phone'));
        $filters['phone'] = $phone;
//        $filters['phone'] = '+380930268764';

        $response = (new FJn)->get_ew_info($filters, $sorting, $page);

        return response()->json($response);
    }


    /**
     * Количество активных входящих и исходящих посылок
     * @return JsonResponse
     */
    public function activeQuantity()
    {
        $quantity = ['incoming' => 0, 'outcoming' => 0];
        try{
            $response = (new FJn)->get_ew_active_quantity();
            if( $response->status ){
                foreach ($response->result->ews as $item){
                    $key = (int)$item->is_incoming === 0 ? 'outcoming' : 'incoming';
                    $quantity[$key]++;
                }
            }
        } catch (\Exception $e){ }

        return response()->json($quantity);
    }


    /**
     * @param Request $request
     *
     * @return mixed
     */
    public function item(Request $request)
    {
        $ew_number = $request->ew_number;

        return $ew_number;
    }

    /**
     * @param Request $request
     *
     * @return mixed
     */
    public function getStatuses(Request $request)
    {
        return Status::get();
    }
}
