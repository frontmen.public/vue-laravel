<?php

namespace App\Http\Controllers;

use App\FJn;
use Illuminate\Http\Request;
use App\UserProfile;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Mockery\Exception;

class ProfileController extends Controller
{
    public function index()
    {
        return [
            'memberId' => session('memberId'),
            'email' => session('email'),
            'gender' => session('gender'),
            'birthday' => session('birthday'),
            'last_name' => session('last_name'),
            'first_name' => session('first_name'),
            'middle_name' => session('middle_name'),
            'phone' => session('phone'),
        ];
    }

    public function update(Request $request)
    {
        $rules = [
            'email' => 'nullable|email',
            'birthday' => 'nullable|date|before_or_equal:' . \Carbon\Carbon::now()->subYears(16)->format('Y-m-d'),
        ];

        $msg = [
//            'birthday.required' => 'Вкажіть, будь ласка, дату народження',
        ];

        $input = $request->all();
        if ($request->has('phone')) {
            $input['phone'] = preg_replace('/[^\d+]/', '', $input['phone']);
        }

        $validator = Validator::make($input, $rules, $msg);
        if ($validator->fails()) {
            return [
                'is_valid' => false,
                $validator->errors()
            ];
        } else {
            $responce = (new FJn)->update(session('memberId'), $input);
            if ($responce->status) {
                Session::put($input);
                return [
                    'is_valid' => true,
                ];
            }
        }
    }

    public function showLogin()
    {
        return view('profile.login');
    }

    public function doLogin(Request $request)
    {
        $rules = [
            'phone' => 'required|phone:UA',
            'password' => 'required|min:6'
        ];

        $msg = [
            'phone.required' => 'Необхідно вказати номер телефону',
            'password.required' => 'Поле пароль обов`язкове до заповнення',
            'password.min' => 'Пароль не може містити менше 6 символів',
            'password.max' => 'Пароль не може містити більше 12 символів',
        ];

        $input = $request->all();
        $validator = Validator::make($input, $rules, $msg);

        if ($validator->fails())
            return [
                'is_valid' => false,
                'registration' => 0,
                $validator->errors()
            ];

        $responce = (new FJn)->login($request->phone, $request->password);

        if (!$responce->status) {
            // если номера телефона нет в БД
            if ($responce->msg->code === 60212) {
                return [
                    'is_valid' => false,
                    'registration' => 1,
                    ['common' => $responce->msg->ua]
                ];
            }
            return [
                'is_valid' => false,
                'registration' => 0,
                ['common' => $responce->msg->ua]
            ];
        } else {
            Session::put((array)$responce->result);
            return [
                'is_valid' => true,
                'result' => $responce->result
            ];
        }
    }

    public function logout()
    {
        auth()->logout();
        session()->invalidate();

        return redirect('/');
    }


    public function forgot()
    {
        return view('profile.forgot');
    }

    public function sendCode(Request $request)
    {
        // TODO add unique phone number validation
        $rules = [
            'phone' => 'required|phone:UA',
        ];

        $input = $request->all();
        $phone = preg_replace('/[^\d+]/', '', $input['phone']);
        $validator = Validator::make($input, $rules);
        if ($validator->fails()) {
            return [
                'is_valid' => false,
                $validator->errors()
            ];
        }

        try {
            $responce = (new FJn)->checkPhone($phone);
            if (!$responce->status) {
                throw new Exception($responce->msg->ua);
            }
            Session::put('tmid', $responce->result->memberId);
            $responce = (new FJn)->verifyPhone($phone);
        } catch (\Exception $e) {
            return [
                'is_valid' => false,
                'is_unknown' => true,
                ['phone' => [$e->getMessage()]]
            ];
        }

        if ($responce->status && $responce->result->verify_code) {
            Session::put([
                'tphone' => $request->phone,
                'verify_code' => $responce->result->verify_code
            ]);

            return [
                'is_valid' => true,
//                'c' => $responce->result->verify_code
            ];

        } else {
            return [
                'is_valid' => false
            ];
        }

        return [
            'is_valid' => true,
        ];
    }

    public function validateCode(Request $request)
    {
        //TODO add code string length validation
        $rules = [
            'code' => 'required',
        ];

        $this->validate($request, $rules);
        $input = $request->all();

        if ($input['code'] == session('msg')) {
            $phone = session('tphone');
            $profile = UserProfile::where('phone', $phone)->first();
            Auth::login($profile->user);

            return view('profile/password');
        } else {
            return back()->with('alert-error', __('msg.code_invalid'));
        }
    }

    public function changePassword(Request $request)
    {
        $rules = [
            'password' => 'required|min:6|same:c_password',
            'c_password' => 'required',
        ];

        $input = $request->all();
        $validator = Validator::make($input, $rules);

        if ($validator->fails()) {
            return [
                'is_valid' => false,
                $validator->errors()
            ];
        }

        $responce = (new FJn)->update(Session::get('tmid'), ['pass' => $request->password]);
        if ($responce->status) {
            $info = (new FJn)->info(Session::get('tmid'));
            Session::put((array)($info->result));
            return [
                'is_valid' => true,
            ];
        }
    }

    public function changeOldPassword(Request $request)
    {
        $rules = [
            'password' => 'required|min:6|same:c_password',
            'c_password' => 'required|same:password',
            'o_password' => 'required',
        ];

        $msg = [
            'password.required' => 'Пароль пароль обов`язкове до заповнення',
            'c_password.required' => 'Пароль новий пароль обов`язкове до заповнення',
            'o_password.required' => 'Пароль підтвердіть пароль обов`язкове до заповнення',
            'password.min' => 'Поле пароль не повинно бути менше 6 символів',
            'password.max' => 'Довжина поля пароль не може перевищувати 12 символів.',
//            'password.same' => ''
        ];

        $input = $request->all();
        $validator = Validator::make($input, $rules, $msg);

        if ($validator->fails())
            return [
                'is_valid' => false,
                $validator->errors()
            ];

        $responce = (new FJn)->login(Session::get('phone'), $request->o_password);

        if (!$responce->status) {
            return [
                'is_valid' => false,
                ['o_password' => [$responce->msg->ua]]
            ];
        } else {
            $responce = (new FJn)->update(Session::get('memberId'), ['pass' => $request->password]);
            return [
                'is_valid' => true,
                []
            ];
        }


//        $user = Auth::user();

//        $phone = $user->profile->phone;
//        $id = $user->id;

//        $creds = [
//            'id' => $id,
//            'password' => $request->o_password
//        ];

//        if(Auth::validate($creds))
//        {
//            $user->password = Hash::make($request->password);
//            // Auth::logout();
//
//            if($user->save())
//                return [
//                    'is_valid' => true
//                ];
//        }
        // return redirect()->route('profile');
    }

    public function changeOldPhone($old_phone, $new_phone)
    {

    }
}
