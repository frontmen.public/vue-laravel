<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Feedback;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class FeedbackController extends Controller
{
    public function create(Request $request)
    {
        $rules = [
            'topic_id' => 'required',
            'text' => 'required',
            'way' => 'required',
            'name' => 'required',
        ];
        $contact = null;
        switch ($request->way){
            case 'телефон':
                $rules['phone'] = 'required|phone:UA';
                $contact = ((strlen($request->phone) == 9)
                    ? '+380'
                    : '').$request->phone;
                break;
            case 'email':
                $rules['email'] = 'required|email';
                $contact = $request->email;
                break;
            default:
                $contact = '';
                break;
        }


        $msg = [
            'text.required' => 'Введіть текст повідомлення',
            'topic_id.required' => 'Оберіть тему',
            'way.required' => 'Оберіть метод сповіщення',
            'phone.required' => 'Обов\'язково вкажіть телефон або оберіть інший спосіб інформування',
            'phone.phone:UA' => 'Невалідний формат телефону',
            'email.required' => 'Обов\'язково вкажіть email або оберіть інший спосіб інформування',
            'email.email' => 'Невалідний формат email',
            'name.required' => 'Вкажіть, будь ласка, ім\'я',
        ];

        $input = $request->all();

        $validator = Validator::make($input, $rules, $msg);

        if($validator->fails())
            return [
                'is_valid' => false,
                $validator->errors()
            ];
        else{

            $feed = [
            ];

            $to = $feed[$request->topic_id]['to'];
            $theme = $feed[$request->topic_id]['theme'];

            $sbj = 'Заявка на обратную связь';

            $headers = [
                'From: office@',
                'Content-Type: text/html; charset="UTF-8";',
            ];

            $msg = "";
            if(isset($request->name)){
                $msg .= "ПІБ: ".$request->name . " <br>";
            }
            if($request->way == 'телефон' || $request->way == 'email') {
                $msg .= "Відповісти потрібно на $contact <br>";
            } else {
                $msg .= "Зв`язуватись не потрібно <br>";
            }

            $msg .= "Тема: $theme <br>";
            $msg .= 'Текст повідомлення: '.$request->text . '<br>';
            $msg .= 'Вказаний телефон у профілі - '.Session::get('phone');

            return [
                'is_valid' => mail(env('MAIL_TO_FEEDBACK').', '.$to.', ilya@frontmen.fm',  $sbj, $msg, implode("\n", $headers)),
                ['error' => 'Не удалось отправить E-mail']
            ];
        }
    }
}
