<?php

namespace App\Http\Controllers;

use App\City;

/**
 * Class CityController
 * @package App\Http\Controllers
 */
class CityController extends Controller
{
    /**
     * Show list of cites that has fetched via cron from the API.
     *
     * @Route('/city', methods="POST", name="city")
     *
     * @return mixed
     */
    public function index()
    {
        $data = City::orderBy('name_ua', 'ASC')
                    ->with(['region', 'departments'])
                    ->has('departments')
                    ->has('region')
                    ->get();

        return $data;
    }
}
