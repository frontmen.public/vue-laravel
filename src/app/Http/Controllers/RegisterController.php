<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Session;
use Symfony\Component\VarDumper\VarDumper;

class RegisterController extends Controller
{
    const VIEW_MAP = [
        1 => 'fstep',
        2 => 'sstep',
        3 => 'tstep'
    ];

    public function step($step)
    {
        $view = 'register.step.' . self::VIEW_MAP[$step];
        return view($view);
    }

//     public function sendCode(Request $request)
//     {
//         // TODO add unique phone number validation
//         $rules = [
//             'phone' => 'required|unique:user_profile,phone|phone:UA',
// //            'g-recaptcha-response'=>'required|recaptcha'
//        ];

//         $this->validate($request, $rules);
//         $phone = str_replace(' ', '', $request->phone);

//         $request->session()->put('phone', $phone);
// //        User::sendSms();

//         return redirect('register/step/2');
//     }

    // public function validateCode(Request $request)
    // {
    //     //TODO add code string length validation
    //     $rules = [
    //         'code' => 'required',
    //     ];

    //     $this->validate($request, $rules);
    //     $input = $request->all();

    //     if($input['code'] == $request->session()->get('msg'))
    //         return redirect('register/step/3');
    //     else{
    //         $request->session()->flash('alert-error', __('msg.code_invalid'));
    //         return back();
    //     }

    // }

    public function register(Request $request)
    {
        $rules = [
            'password' => 'required|min:6|same:c_password',
            'c_password' => 'required',
            'confirm_agreement' => 'required|accepted'
        ];

        $msg = [
            'confirm_agreement.accepted' => 'Необхідно погодитись з умовами надання послуг',
            'password.min' => 'Пароль не може містити менше 6 символів',
            'password.max' => 'Пароль не може містити більше 12 символів',
        ];

        $input = $request->all();
        $validator = Validator::make($input, $rules, $msg);
        if ($validator->fails()) {
            return [
                'is_valid' => false,
                $validator->errors()
            ];
        } else {
            $response = (new FJn)->register(session('tphone'), $request->password);

            if ($response->status) {
                $memberId = $response->result->memberId;
                session()->put('memberId', $memberId);
                session()->put('phone', session('tphone'));

                $result = [
                    'is_valid' => true,
                    'memberId' => $memberId,
                ];
            } else {
                $result = [
                    'is_valid' => false
                ];
            }
            return $result;
        }
        return redirect()->route('profile');
    }

    // api

    public function setPhone(Request $request)
    {
        // TODO put it to PHONE model
        $rules = ['phone' => 'required|phone:UA'];
        $msg = [
            'phone.required' => 'Поле телефон обов\'язкове до заповнення'
        ];

        $input = $request->all();
        $validator = Validator::make($input, $rules, $msg);
        if ($validator->fails()) {
            return [
                'is_valid' => false,
                $validator->errors()
            ];
        }

        // TODO add phone number to session
        // TODO send code to session->phone_number
        $response = (new FJn)->checkPhone($request->phone);

        $is_user_exist = $response->status;
        if ($is_user_exist) {
            return [
                'is_valid' => false,
                [
                    'phone' => ['Користувач вже зареєстрований']
                ],
                'is_exist' => true
            ];
        } else {
            $response = (new FJn)->verifyPhone($request->phone);
            $verify_code = $response->status ? $response->result->verify_code : null;

            Session::put([
                'tphone' => $request->phone,
                'verify_code' => $verify_code
            ]);

            return [
                'is_valid' => true,
//                'c' => $verify_code
            ];
        }
    }

    public function validateCode(Request $request)
    {
        $rules = [
            'code' => 'required',
        ];

        $result = [];

        $input = $request->all();
        $validator = Validator::make($input, $rules);
        if ($validator->fails()) {
            $result = [
                'is_valid' => false,
                $validator->errors()
            ];
        } else {
            $result['is_valid'] = ($request->code == $request->session()->get('verify_code'));
        }

        return $result;
    }
}
// https://medium.com/techcompose/create-rest-api-in-laravel-with-authentication-using-passport-133a1678a876
// https://laravel.com/docs/5.8/passport
