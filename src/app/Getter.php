<?php
namespace App;

use GuzzleHttp\Client;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;

class Getter
{
    /**
     * обновление регионов
     * @return string
     */
    public static function updateRegions()
    {
        $guzzle = new Client([
            'base_uri' => env('_API_BASE'),
            'auth' => [
                env('_API_TEST_LOGIN'),
                env('_API_TEST_PASSWORD')
            ]
        ]);

        $body = [
            'keyAccount' => env('_API_TEST_KEY_ACCOUNT'),
            'sign' => sha1(env('_API_TEST_SIGN').':'.date('Y-m-d')),
            'request' => 'getData',
            'type' => 'catalog',
            'name' => 'cat_Region',
            'language' => 'ua',
        ];


        $languages = Config::get('app.languages');

        $regions = [];

        foreach( $languages as $i=>$lang  ){
            $body = [
                'keyAccount' => env('_API_TEST_KEY_ACCOUNT'),
                'sign' => sha1(env('_API_TEST_SIGN').':'.date('Y-m-d')),
                'request' => 'getData',
                'type' => 'catalog',
                'name' => 'cat_Region',
                'language' => $lang,
            ];
////            var_dump($body);

            $res = $guzzle->post(env('_API_TEST_URI'), [
                'body' => json_encode($body)
            ])->getBody();

            $regions[$lang] = json_decode($res->getContents(), true)['data'];
////            var_dump($regions);

        }

        $map = array_map(function ($ua, $en, $ru) {
            return [
                'uuid' => $ua['fields']['uuid'],
                'name_ua' => $ua['fields']['descr'],
                'name_en' => $en['fields']['descr'],
                'name_ru' => $ru['fields']['descr'],
            ];
        }, $regions['ua'], $regions['en'], $regions['ru']);

        foreach($map as $i => $model){
            Region::updateOrCreate(
                ['uuid' => $model['uuid']],
                $model
            );
        }

        return 'success';
    }


    /**
     * обновление городов
     * @return string
     */
    public static function updateCities()
    {
        try{
            $response = (new F)->localities();
            if( $response->status ){
                $regionIds = Region::get()->pluck('uuid', 'id')->toArray();
                foreach ($response->result as $city){
                    City::updateOrCreate([
                        'uuid' => $city->uuid
                    ], [
                        'region_id' => $regionIds[$city->parent_uuid] ?? 0,
                        'region_uuid' => $city->parent_uuid,
                        'name_ua' => $city->title_ua,
                        'name_ru' => $city->title_ru,
                        'name_en' => $city->title_en
                    ]);
                }
            }
        } catch (\Exception $e ){}
    }


    /**
     * обновление отделений
     * @param Region $region
     * @return string
     */
    public static function updateDepartments(Region $region)
    {
        $guzzle = new Client([
            'base_uri' => env('_API_BASE'),
            'auth' => [
                env('_API_TEST_LOGIN'),
                env('_API_TEST_PASSWORD')
            ]
        ]);

        $languages = Config::get('app.languages');

        $departments = [];

        foreach( $languages as $i=>$lang  ){
            $body = [
                'keyAccount' => env('_API_TEST_KEY_ACCOUNT'),
                'sign' => sha1(env('_API_TEST_SIGN').':'.date('Y-m-d')),
                'request' => 'getData',
                'type' => 'request',
                'name' => 'req_DepartmentsLang',
                'language' => $lang,
                'params' => [
                    'language' => $lang,
                ],
                'filter' => [
                    ["name" => "region",
                    "comparison" => "equal",
                    "leftValue" => $region->uuid]
                ]
            ];
//            var_dump($body);

            $res = $guzzle->post(env('_API_TEST_URI'), [
                'body' => json_encode($body)
            ])->getBody();

            $departments[$lang] = json_decode($res->getContents(), true)['data'];
//            var_dump($departments);
        }

        $map = array_map(function ($ua, $en, $ru) use ($region) {
            return [
                'uuid' => $ua['fields']['Depart']['uuid'],
                'region_id' => $region->id,
                'region_uuid' => $region->uuid,
                'city_uuid' => $ua['fields']['city']['uuid'],
                'weight_limit' => $ua['fields']['weight_limit'],
                'lat' => $ua['fields']['lat'],
                'lng' => $ua['fields']['lng'],
                'department_no' => $ua['fields']['departNumber'],
                'building_no' => $ua['fields']['houseNumber'],
                'street_ua' => $ua['fields']['street']['descr'],
                'street_en' => $en['fields']['street']['descr'],
                'street_ru' => $ru['fields']['street']['descr'],
                'branch' => $ua['fields']['branch'],
            ];
        }, $departments['ua'], $departments['en'], $departments['ru']);

        foreach($map as $i => $model){
//            dd($model);
            $dep = Department::updateOrCreate(
                ['uuid' => $model['uuid']],
                $model
            );
        }
        return 'success';
    }
}
