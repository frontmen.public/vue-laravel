<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use SoapClient;
use Illuminate\Support\Facades\Session;
use Laravel\Passport\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function profile()
    {
        return $this->hasOne(UserProfile::class);
    }

//    static public function sendSms()
//    {
//        $client = new SoapClient ('http://turbosms.in.ua/api/wsdl.html');
//
//        $auth = [
//            'login' => env('TSMS_LOGIN'),
//            'password' => env('TSMS_PASSWORD'),
//        ];
//
//        $msg = rand(10000, 99999);
//        Session::put('msg', $msg);
//
//        $result = $client->Auth($auth);
//
//        $sms = [
//            'sender' => env('TSMS_SIGNATURE'),
//            'destination' => Session::get('phone'),
//            'text' => $msg
//        ];
//        $result = $client->SendSMS($sms);
//        // return true;
//
//        return $result->SendSMSResult->ResultArray;
//    }
}
