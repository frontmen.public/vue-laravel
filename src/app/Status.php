<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Status extends Model
{
    protected $table = 'status';

    protected $fillable = [
        'uuid',
        'description_en',
        'description_ru',
        'description_ua',
        'description',
        'name_ua',
        'name_en',
        'name_ru',
        'code',
        'is_archive',
    ];
}
