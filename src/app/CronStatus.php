<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CronStatus extends Model
{
//    const UPDATED_AT = 'date';
    
    protected $table = 'cron_status';

    protected $fillable = [
        'type',
        'date',
        'status',
        'sity_id',
        'region_id',
    ];
}
