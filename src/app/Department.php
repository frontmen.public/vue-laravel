<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Department extends Model
{
    protected $table = 'department';

    protected $fillable = [
        'uuid',
        'region_id',
        'region_uuid',
        'city_uuid',
        'street_ua',
        'street_en',
        'street_ru',
        'weight_limit',
        'lat',
        'lng',
        'department_no',
        'building_no',
        'branch',
    ];

    public function city()
    {
        return $this->belongsTo(City::class, 'city_uuid', 'uuid');
    }

    public function region()
    {
        return $this->belongsTo(Region::class, 'region_uuid', 'uuid');
    }
}
