<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $table = 'order';

    protected $fillable = [
        'ttn',
        'number',
        'user_id',
        'sender_api_key',
        'receiver_api_key',
        'sender_phone',
        'receiver_phone',
        'body'
    ];
}
