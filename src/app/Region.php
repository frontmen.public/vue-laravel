<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Region extends Model
{
    protected $table = 'region';

    protected $fillable = [
        'uuid',
        'name_ua',
        'name_en',
        'name_ru',
    ];

    public function cities() {
        return $this->hasMany(City::class);
    }
    
//    // bulk insert or update 
//    // TODO make as common
//    public function insertOrUpdate(array $rows = []){
//        $table = \DB::getTablePrefix().with(new self)->getTable();        
//        
//        $first = reset($rows);
//        
//        $columns = implode( ',',
//            array_map( function( $value ) { return "$value"; } , array_keys($first) )
//        );
//        
//        $values = implode( ',', array_map( function( $row ) {
//                return '('.implode( ',',
//                    array_map( function( $value ) { return '"'.str_replace('"', '""', $value).'"'; } , $row )
//                ).')';
//            } , $rows )
//        );
//        
//        $updates = implode( ',',
//            array_map( function( $value ) { return "$value = VALUES($value)"; } , array_keys($first) )
//        );
//        
//        $sql = "INSERT INTO {$table}({$columns}) VALUES {$values} ON DUPLICATE KEY UPDATE {$updates}";
//        
//        return \DB::statement( $sql );
//    }
}
