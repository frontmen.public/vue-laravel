<?
return [
    'phone' => 'phone',
    'submit' => 'submit',
    'code' => 'code',
    'pwd' => 'password',
    'cnfm_pwd' => 'confirm password',
    'name' => 'name',
    'second_name' => 'second name',
    'middle_name' => 'middle name',
    'gender' => 'gender',
    'region' => 'region',
    'sity' => 'sity',
    'street' => 'street',
    'building' => 'building',
    'flat' => 'flat',
    'birth_date' => 'date of birth',
    'register' => 'register',
    'login' => 'login',
    'logout' => 'logout',
    'code_invalid' => 'invalid code',
    'profile' => 'profile',
    'forgot_password' => 'forgot password'
];