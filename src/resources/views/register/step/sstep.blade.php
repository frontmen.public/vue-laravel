@extends('layouts.register')

@section('content')
    @if(Session::has('alert-error'))
        <br>
        {{ Session::get('alert-error') }}
        <br>
    @endif
    {{Form::open(['url' => 'register/validate_code'])}}

        {{Form::label('code', __('msg.code'))}}
        {{Form::text('code')}}

        {{Form::submit(__('msg.submit'))}}
    {{Form::close()}}
@stop