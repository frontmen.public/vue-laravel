<!doctype html>
<html lang="{{ app()->getLocale() }}">
    @include('includes.head')
    <body>
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <div class="register">
            <div id="app" class="">
                @include('includes.left')
                <router-view></router-view>
                @include('includes.footer')
            </div>
        </div>
    </body>
    @include('includes.after_body')
</html>
