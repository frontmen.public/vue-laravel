<!doctype html>
<html lang="{{ app()->getLocale() }}">
    @include('includes.head')
    <body>
        @if ($errors->any())
            <div class="alert alert-danger">
                <div>
                    @foreach ($errors->all() as $error)
                        <div>{{ $error }}</div>
                    @endforeach
                </div>
            </div>
        @endif
        <div id="app">
            @include('includes.left')

            <router-view></router-view>

            @include('includes.footer')
        </div>
        <script src="/js/app.js?v=1.10" defer></script>
    </body>
    @include('includes.after_body')
</html>

