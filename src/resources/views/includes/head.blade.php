<head>
    <meta name="description" content="" />
    <meta name="keywords" content="" />
    <meta name="robots" content="index,follow" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" value="{!! csrf_token() !!}">

    <link rel="icon" type="image/png" href="/32x32_j_krug.png">
    <link rel="preload" href="/css/app.css?v=1.4" as="style" onload="this.rel='stylesheet'">
</head>
