@extends('layouts.register')

@section('content')
    {{Form::open(['url' => '/profile/change_password'])}}

        {{Form::label('pwd', __('msg.pwd'))}}
        {{Form::password('pwd')}}

        {{Form::label('cnfm_pwd', __('msg.cnfm_pwd'))}}
        {{Form::password('cnfm_pwd')}}

        {{Form::submit(__('msg.submit'))}}
    {{Form::close()}}
@stop
