@extends('layouts.main')

@section('content')
<div class='row'>
    <div class='col-md-4'>
        {{$model->id}}
    </div>
    <div class='col-md-4'>
        {{$model->password}}
    </div>
    <div class='col-md-4'>
        {{$model->profile->phone}}
    </div>
    <div class='col-md-4'>
        {{$model->profile->created_at}}
    </div>
    <div class='col-md-4'>
        {{$model->profile->updated_at}}
    </div>
</div>
@stop