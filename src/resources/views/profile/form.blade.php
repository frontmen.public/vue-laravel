{{Form::model($model, ['route' => 'profile_update'])}}

        {{Form::hidden('id')}}

        {{Form::label('name', __('msg.name'))}}
        {{Form::text('name')}}

        {{Form::label('second_name', __('msg.second_name'))}}
        {{Form::text('second_name')}}
        
        {{Form::label('middle_name', __('msg.middle_name'))}}
        {{Form::text('middle_name')}}
        
        {{Form::label('phone', __('msg.phone'))}}
        {{Form::tel('phone')}}
        
        {{Form::label('gender', __('msg.gender'))}}
        {{Form::text('gender')}}
        
        {{Form::label('region', __('msg.region'))}}
        {{Form::text('region')}}
        
        {{Form::label('sity', __('msg.sity'))}}
        {{Form::text('sity')}}
        
        {{Form::label('street', __('msg.street'))}}
        {{Form::text('street')}}
        
        {{Form::label('building', __('msg.building'))}}
        {{Form::text('building')}}
        
        {{Form::label('flat', __('msg.flat'))}}
        {{Form::text('flat')}}
        
        {{Form::label('birth_date', __('msg.birth_date'))}}
        {{Form::text('birth_date')}}

        {{Form::submit(__('msg.submit'))}}
{{Form::close()}}