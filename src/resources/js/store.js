import Vue from 'vue';
import Vuex from 'vuex';
import VuexPersistence from 'vuex-persist';

Vue.use(Vuex);
const vuexLocal = new VuexPersistence({
    storage: window.sessionStorage
});

export default new Vuex.Store({
    state: () => ({
        phone_is_valid: false,
        code_is_valid: false,
        password_is_valid: false,
        phone: '',
        phone_is_exist: false,
        phone_is_unknown: false,
        phone_err_msg: '',
        password_errors: {},
        user: {
            user: {},
            profile: {}
        },
        locale: 'ua',
        profile_validation_messages: {},
        is_guest: false,
        is_user: false,
        cities: [],
        topics: [],
        departments: [],
        orderStatus: {},
        package_created: false,
        errors: {},
        is_success: false,
        rules: {
            required: value => !!value || 'Required.',
            min: v => v.length >= 2 || 'Min 2 characters',
            max: v => v.length <= 16 || 'Max 16 characters',
            email: value => {
                if(value.length){
                    const pattern = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
                    return pattern.test(value) || 'Invalid e-mail.'
                }
            }
        },
        clicked: 0,
        weight_error: null,
        length_error: null,
        desc_error: null,
        declared_cost_error: null,
        ttn: null,
        orders: {},
        pagination: {},
        statuses: {},
		loading: true,
        isIncoming: 0,
        isArchive: 0
    }),
    getters: {
        phone_is_valid: state => {
            return state.phone_is_valid;
        },
        phone_is_exist: state => {
            return state.phone_is_exist;
        },
        phone_is_unknown: state => {
            return state.phone_is_unknown;
        },
        phone_err_msg: state => {
            return state.phone_err_msg;
        },
        code_is_valid: state => {
            return state.code_is_valid;
        },
        password_is_valid: state => {
            return state.password_is_valid;
        },
        password_errors: state => {
            return state.password_errors;
        },
        user: state => {
            return state.user;
        },
        profile_validation_msg: state => {
            return state.profile_validation_messages;
        },
        is_guest: state => {
            return state.is_guest
        },
        is_user: state => {
            return state.is_user;
        },
        cities: state => {
            return state.cities;
        },
        topics: state => {
            return state.topics;
        },
        departments: state => {
            return state.departments;
        },
        errors: state => {
            return state.errors;
        },
        is_success: state => {
            return state.is_success;
        },
        points: state => {
            return state.points;
        },
        phone: state => {
            return state.phone;
        },
        clicked: state => {
            return state.clicked;
        },
        package_created: state => {
            return state.package_created;
        },
        weight_error: state => {
            return state.weight_error;
        },
        length_error: state => {
            return state.length_error;
        },
        declared_cost_error: state => {
            return state.declared_cost_error;
        },
        desc_error: state => {
            return state.desc_error;
        },
        ttn: state => {
            return state.ttn;
        },
        en: state => {
            return state.en;
        },
        orders: state => {
            return state.orders;
        },
        pagination: state => {
            return state.pagination
        },
        statuses: state => {
            return state.statuses
        },
		is_load: state => {
			return state.loading;
		}
    },
    mutations: {
        set_errors: (state, errors) => {
            state.errors = errors;
        },
        set_success: (state, data) => {
            state.is_success = data;
        },
        set_phone: (state, phone) => {
            state.phone = phone;
        },
        set_phone_status: (state, is_valid) => {
            state.phone_is_valid = is_valid;
        },
        set_phone_is_exist: (state, is_exist) => {
            state.phone_is_exist = is_exist;
        },
        set_phone_is_unknown: (state, is_exist) => {
            state.phone_is_unknown = !is_exist;
        },
        set_phone_err_msg: (state, msg) => {
            state.phone_err_msg = msg;
        },
        set_code_status: (state, is_valid) => {
            state.code_is_valid = is_valid;
        },
        set_password_status: (state, is_valid) => {
            state.password_is_valid = is_valid;
        },
        set_password_err: (state, errors) => {
            state.password_errors = errors;
        },
        set_profile: (state, user) => {
            state.user = user;
            // state.user
        },
        set_profile_validation: (state, {key, value}) => {
            state.profile_validation_messages[key] = value[0];
        },
        is_guest: (state, is_guest) => {
            state.is_guest = is_guest;
        },
        set_is_user: (state, is_user) => {
            state.is_user = is_user;
        },
        setCities: (state, cities) => {
            state.cities = cities;
        },
        setTopics: (state, topics) => {
            state.topics = topics;
        },
        setDepartments: (state, departments) => {
            state.departments = departments;
        },
        setOrderStatus: (state, status) => {
            state.package_created = Boolean(status.ttn);
            state.ttn = status.ttn;
            state.en = status.number;
            state.orderStatus = status;
        },
        refreshOrdertStatus: (state) => {
            state.package_created = false;
        },
        setPoints: (state, points) => {
            state.points = points;
        },
        refreshErrors: state => {
            state.phone_err_msg = '';
            state.phone_is_exist = false;
            state.phone_is_valid = false;
        },
        handleClick: state => {
            state.clicked++;
        },
        setWeightError: (state, data) => {
            state.weight_error = data;
        },
        setLengthError: (state, data) => {
            state.length_error = data;
        },
        setDeclaredCostError: (state, data) => {
            state.declared_cost_error = data;
        },
        setDescError: (state, data) => {
            state.desc_error = data;
        },
        setOrders: (state, data) => {
            state.orders = data;
        },
        // setOrder: (state, data) => {
        //     state.orders = data;
        // },
        setPagination: (state, data) => {
            state.pagination = data;
        },
        setStatuses: (state, data) => {
            state.statuses = data;
        },
		is_loading: (state, data) => {
			state.loading = data;
		},
        setIsIncoming: (state, data) => {
            state.isIncoming = data;
        },
        setIsArchive: (state, data) => {
            state.isArchive = data;
        }
    },
    actions: {
        setPoints: (state, points) => {
            state.commit('setPoints', points)
        },
        set_phone: (state, phone) => {
            state.commit('set_phone', phone)
        },
        validate_phone_forgot: (state, phone) => {
            axios
                .post('/forgot', {phone: phone})
                .then(response => {

                    // BOOL response.data.is_valid - required
                    // STRING response.data[0].phone[0] - optional (if !is_valid)

                    if(response.data.is_unknown)
                    {
                        // state.commit('set_phone_is_unknown', response.data.unknown);
                        state.commit('set_phone_err_msg', response.data.unknown);
                    }
                    if(!response.data.is_valid)
                    {
                        state.commit('set_phone_err_msg', response.data[0].phone[0]);
                    }
                    state.commit('set_phone_status', response.data.is_valid);
                })
        },
        validate_phone: (state, phone) => {
            axios
                .post('/register/set_phone', {phone: phone})
                .then(response => {

                    // BOOL response.data.is_valid - required
                    // STRING response.data[0].phone[0] - optional (if !is_valid)
                    if(!response.data.is_valid)
                    {
                        state.commit('set_phone_err_msg', response.data[0].phone[0]);
                        if(response.data.is_exist)
                        {
                            state.commit('set_phone_is_exist', response.data.is_exist);
                        }
                    }
                    state.commit('set_phone_status', response.data.is_valid);
                })
        },
        reset_phone: (state) => {
            state.commit('set_phone_status', false);
        },
        change_old_password: (state, {password, c_password, o_password}) => {
            axios
                .post('/profile/change_old_password', {
                    password: password,
                    c_password: c_password,
                    o_password: o_password,
                })
                .then(response => {
                    if(response.data[0])
                    {
                        state.commit('set_password_status', response.data.is_valid);
                        state.commit('set_errors', response.data[0]);
                    }
                    else
                    {
                        router.push('/login');
                    }
                })
        },
        validate_user: (state, {phone, password} ) => {
            axios
                .post('/login', {phone: phone, password: password})
                .then(response => {
                    if(response.data.is_valid) router.push('/profile');
                })
        },
        set_profile: state => {
            axios
                .post('/profile')
                .then(response => {
                    state.commit('set_profile', response.data);
                    // 19:22 TODO
                    state.commit('is_guest', !(response.data.memberId) );
                })
        },
        set_is_user: (state,{is_user}) => {
            state.commit('set_is_user', is_user);
        },
        update_profile: (state, {
            last_name,
            first_name,
            middle_name,
            phone,
            email,
            gender,
            birthday,
            // sity,
            // street,
            // building,
            // flat,
            accept,
        }) => {
            // console.log('old', state.state.user.first_name, 'new', first_name);
            // return ;
            axios
                .post('/profile_update', {
                    last_name: last_name,
                    first_name: first_name,
                    middle_name: middle_name,
                    phone: phone,
                    email: email,
                    gender: gender,
                    birthday: birthday,
                    // sity: sity,
                    // street: street,
                    // building: building,
                    // flat: flat,
                    accept: (accept == 'on' || accept)
                })
                .then(response => {
                    let {data} = response;
                    if(!data.is_valid)
                    {
                        let key = Object.keys(data[0])[0];
                        let value = data[0][key];
                        state.commit('set_profile_validation', {key, value});
                        state.commit('set_errors', data[0]);
                        // alert('fail');
                    } else {
                        state.commit('set_success', data.is_valid);
                        state.commit('set_errors', {});
                    }
                })
        },
        refresh_is_success: (state) => {
            state.commit('set_success', false);
        },
        set_locale: (state, locale) => {
            i18n.locale = locale;
        },
        setCities: state => {
            axios
                .post('/city')
                .then(response => {
                    state.commit('setCities', response.data);
                })
        },
        setTopics: state => {
            axios
                .post('/topic')
                .then(response => {
                    state.commit('setTopics', response.data);
                })
        },
        setDepartments: (state, city_uuid) => {
            axios
                .post('/department', {city_uuid: city_uuid})
                .then(response => {
                    state.commit('setDepartments', response.data);
                })
        },
        // setOrders: (state, data) => {
        //     axios
        //         .post('/order/list', data)
        //         .then(response => {
        //             let {data} = response;
        //             state.commit('setOrders', data.result ? data.result.ews : {});
        //             state.commit('setPagination', data.result ? data.result.pagination : {});
		// 			state.commit('is_loading', false)
        //         })
        //         .catch(
        //             error => {
        //                 console.log(error)
        //             }
        //         )
        // },
        // setOrder: (state, data) => {
        //     axios
        //         .post('/order/item', data)
        //         .then(responce => {
        //             console.log(responce);
        //         })
        // },
        setStatuses: (state, data) => {
            axios
                .post('/order/getStatuses', data)
                .then(response => {
                    let {data} = response;
                    state.commit('setStatuses', data);
                    // console.log(data)
                    // if(data.result){
                    // }
                })
                .catch(
                    error => {
                        console.log(error)
                    }
                )
        },
        setWeightError: (state, error) => {
            state.commit('setWeightError', error);
        },
        setLengthError: (state, error) => {
            state.commit('setLengthError', error);
        },
        setDeclaredCostError: (state, error) => {
            state.commit('setDeclaredCostError', error);
        },
        setDescError: (state, error) => {
            state.commit('setDescError', error);
        },
        handleClick: (state) => {
            state.commit('handleClick');
        },
        setTTN: (state, ttn) => {
            state.commit('setTTN', ttn);
        },
        setOrderStatus: (state, data) => {
            state.commit('setOrderStatus', data);
        }

    },
    plugins: [vuexLocal.plugin]
});
