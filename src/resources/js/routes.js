module.exports = [
    // {
    //  path: '/',
    //     component: require('./containers/Login.vue').default,
    //     redirect: '/login',
    //     children: [
    //         { path: 'login', component: require('./components/LoginComponent.vue').default, meta: {requiresGuest: true} },
    //         { path: 'register', component: require('./components/RegistrationComponent.vue').default, meta: {requiresGuest: true} },
    //      { path: 'reset', component: require('./components/ResetComponent.vue').default, meta: {requiresGuest: true} }
    //     ],
    //     // meta: { requiresGuest: true }
    // },
    // {
    //     path: '/',
    //     // beforeEnter: (t,f,n) => {
    //     //     return;
    //     //     console.log(t,f)
    //     // }
    // },
    {
        path: '/',
        name: 'root'
    },
    {
        path: '/login',
        component: require('./containers/Login.vue').default,
        children: [{ path: '/login', component: require('./components/LoginComponent.vue').default, meta: {requiresGuest: true} },],
    },
    {
        path: '/register',
        component: require('./containers/Login.vue').default,
        children: [{ path: '/register', component: require('./components/RegistrationComponent.vue').default, meta: {requiresGuest: true} },],
    },
    {
        path: '/reset',
        component: require('./containers/Login.vue').default,
        children: [{ path: '/reset', component: require('./components/ResetComponent.vue').default, meta: {requiresGuest: true} },],
    },
    {
        path: '/profile',
        component: require('./containers/Profile.vue').default,
        children: [
            {path: '/profile', component: require("./components/ProfileUpdateComponent.vue").default},
            {path: '/profile/update/password', component: require('./components/ProfileUpdatePasswordComponent.vue').default},
        ],
        meta: { requiresAuth: true }
    },
    {
        path: '/main',
        component: require('./containers/Profile.vue').default,
        children: [{path: '/', component: require("./components/ProfileMainComponent.vue").default},],
        meta: { requiresAuth: true }
    },
    {
        path: '/feedback',
        component: require('./containers/Package.vue').default,
        children: [
            {
                path: '/',
                component: require('./components/Feedback.vue').default
            }
        ],
        meta: {  requiresAuth: true }
    },
    {
        path: '/info',
        component: require('./containers/Package.vue').default,
        children: [
            {
                path: '/',
                component: require('./components/Info.vue').default
            }
        ],
        meta: { requiresAuth: true }
    },
    {
        path: '/calculator',
        component: require('./containers/Package.vue').default,
        children: [
            {
                path: '/',
                component: require('./components/CalculatorComponent.vue').default
            }
        ],
        meta: { requiresAuth: true }
    },
    {
        path: '/package',
        component: require('./containers/Package.vue').default,
        children: [
            { path: '/package/create', component: require('./components/Package/Create.vue').default },
            { path: '/package/success', component: require('./components/Package/Success.vue').default },
        ],
        meta: { requiresAuth: true }
    },
    {
        path: '/orders',
        component: require('./containers/Orders.vue').default,
        children: [
            {path: '/', component: require("./components/Orders.vue").default},
            {path: '/orders/:ew_number', component: require("./components/Order.vue").default, name: 'orderDetails'}
        ],
        meta: { requiresAuth: true }
    },
    { path: '*', name: '404', component: require('./components/PageNotFound.vue').default }
    //{ path: '/orders', component: require('./components/Orders.vue').default},
];
