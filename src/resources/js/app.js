import 'babel-polyfill';
import Vuex from 'vuex';
import VueRouter from 'vue-router';
import VueI18n from 'vue-i18n';
import VModal from 'vue-js-modal';
import {messages} from '../json/messages';
import VueCookie from 'vue-cookie';
import {SwipeList, SwipeOut} from 'vue-swipe-actions';
import vuescroll from 'vue-scroll';
import store from './store';

require('./bootstrap');

window.Vue = require('vue');

Vue.use(vuescroll);
Vue.use(VModal);
Vue.use(Vuex);
Vue.use(VueRouter);
Vue.use(VueI18n);
Vue.use(VueCookie);

// const files = require.context('./', true, /\.vue$/i);
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default));

Vue.component('lang-selector', require('./components/LangSelectorComponent.vue').default);
Vue.component('menu-component', require('./components/Menu.vue').default);

const routes = require('./routes');
const router = new VueRouter({
    routes: routes,
    mode: 'history'
});
router.beforeEach(async (t, f, n) => {
    // var dbg=0;if(dbg){console.log('____');console.log("_TO");console.log(t);console.log("_FROM");console.log(f);console.log('____');} // DEBUG
    // let is_user=0; // DEBUG

    let {data} = await axios.post('/profile');
    let is_user = Boolean(data.memberId);

    store.commit('set_is_user', is_user);

    // REDIRECT FROM MAIN TO DIFF ROLE // ROOT url used for redirecting behavior users and guests
    if (t.name == 'root') {
        return n(is_user ? '/main' : '/login')
    }

    let is_allow = (is_user && t.matched.some(r => r.meta.requiresAuth))
                || (!is_user && t.matched.some(r => r.meta.requiresGuest))
                || (t.name == '404');

    if( ['/login', '/register'].indexOf(t.path) === -1 ){
        const seo = {
            title: '',
            description: '',
            keywords: '',
        }
        document.title = seo.title;
        document.querySelector('head meta[name="description"]').setAttribute('content', seo.description);
        document.querySelector('head meta[name="keywords"]').setAttribute('content', seo.keywords);
    }

    return is_allow ? n() : n('/');
});

const i18n = new VueI18n({
    locale: 'ua',
    messages
});

const app = new Vue({
    el: '#app',
    store,
    router,
    i18n,
    SwipeOut,
    SwipeList
});
