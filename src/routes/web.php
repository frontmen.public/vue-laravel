<?php
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/* TODO Перейти на SPA */
/* TODO Маршруты для получения данных перенести в API Routes */


Route::get('/', function () {

    return view('welcome');
});

Route::get('logout', 'ProfileController@logout')
    ->name('logout');

Route::get('/{any}', function () {
    return view('welcome');
})->where('any', '.*');

Route::prefix('register')->group(function () {
    Route::get('/', function () {
        return redirect('register/step/1');
    });

    Route::post('set_phone', 'RegisterController@setPhone');
    Route::post('validate_code', 'RegisterController@validateCode');
    Route::post('/', 'RegisterController@register');

    Route::get('step/{step}', 'RegisterController@step');
});
// Route::get('/', 'ProfileController@index');
    // ->middleware('auth');

Route::post('profile', 'ProfileController@index')
    // ->middleware('auth')
    ->name('profile');

Route::post('city', 'CityController@index')
    ->name('city');

Route::post('topic', 'TopicController@index')
    ->name('topic');

Route::post('department', 'DepartmentController@index')
    ->name('department');

Route::prefix('order')->group(function () {
    Route::post('create', 'OrderController@create');
    Route::post('calculate', 'OrderController@calculate');
    Route::post('list', 'OrderController@list');
    Route::post('active-quantity', 'OrderController@activeQuantity');
    Route::post('item', 'OrderController@item');
    Route::post('getStatuses', 'OrderController@getStatuses');
});

Route::post('feedback/create', 'FeedbackController@create');

Route::post('profile_update', 'ProfileController@update')
    // ->middleware('auth')
    ->name('profile_update');

Route::get('login', 'ProfileController@showLogin')
    ->name('login');

Route::post('login', 'ProfileController@doLogin');

Route::get('forgot', 'ProfileController@forgot')
    ->name('forgot');

Route::post('forgot', 'ProfileController@sendCode');

Route::post('profile/validate_code', 'ProfileController@validateCode');
Route::post('profile/change_password', 'ProfileController@changePassword');
Route::post('profile/change_old_password', 'ProfileController@changeOldPassword');

/** Калькулятор стоимости доставки */
// Route::get('/calculator', 'CalculatorController@index')
    // ->middleware('auth')
    // ->name('calculator');

Route::prefix('getter')->group(function () {
    Route::get('regions/{uuid}', 'GetterController@regions');
});
