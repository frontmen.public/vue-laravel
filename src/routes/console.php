<?php

use App\Getter;
use App\CronStatus;
use App\Region;
use App\Status;

/*
|--------------------------------------------------------------------------
| Console Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of your Closure based console
| commands. Each Closure is bound to a command instance allowing a
| simple approach to interacting with each command's IO methods.
|
*/

Artisan::command('get_statuses', function () {

    $response = (new \App\FJn)->get_statuses_list();

    if( $response->status ){
        foreach ($response->result->statuses as $status){
            Status::updateOrCreate([
                'uuid' => $status->uuid
            ], [
                'code' => $status->code,
                'description' => $status->description,
                'is_archive' => $status->is_archive,
                'name_ua' => $status->platforms->forservices->name_ua ?? null,
                'name_ru' => $status->platforms->forservices->name_ru ?? null,
                'name_en' => $status->platforms->forservices->name_en ?? null,
                'description_ua' => $status->platforms->forservices->description_ua ?? null,
                'description_ru' => $status->platforms->forservices->description_ru ?? null,
                'description_en' => $status->platforms->forservices->description_en ?? null,
            ]);
        }
    }
});


// обновление локаций
Artisan::command('getter', function () {

    $is_region_updated_today = CronStatus::where('date', '>', DB::raw('CURDATE()'))
        ->where('type', 'region')
        ->where('status', 'success')
        ->get()
        ->count();

    if (!$is_region_updated_today) {
        $current_task = CronStatus::create(
            [
                'date' => DB::raw('NOW()'),
                'status' => 'in progress',
                'type' => 'region'
            ]
        );
        $status = Getter::updateRegions();

        // TODO can be moved into getter update method
        CronStatus::where('id', $current_task->id)->update([
            'date' => DB::raw('NOW()'),
            'status' => $status,
        ]);
    }
    $regions = Region::get();

    foreach ($regions as $i => $region) {
        // is_sity_by_current_region_updated today

        $is_department_by_current_region_updated = CronStatus::where('date', '>', DB::raw('CURDATE()'))
            ->where('type', 'department')
            ->where('status', 'success')
            ->where('region_id', $region->id)
            ->get()
            ->count();

        if (!$is_department_by_current_region_updated) {
            $current_task = CronStatus::create(
                [
                    'type' => 'department',
                    'date' => DB::raw('NOW()'),
                    'status' => 'in progress',
                    'region_id' => $region->id,
                ]
            );
            $status = Getter::updateDepartments($region);

            // TODO can be moved into getter update method
            CronStatus::where('id', $current_task->id)
                ->update([
                    'date' => DB::raw('NOW()'),
                    'status' => $status,
                ]);
        } else {
            continue;
        }
    }

    // обновление городов
    Getter::updateCities();

})->describe('__Cron getter command__{CUSTOM}');


//Artisan::command('updateCities', function () {
//    // обновление городов
//    Getter::updateCities();
//});
